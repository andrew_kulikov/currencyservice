package com.company.currencyservice.service;

public interface CurrencyService {
    String NAME = "currencyservice_CurrencyService";

     Double getCurrency(String from, String to);
}