package com.company.currencyservice.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service(CurrencyService.NAME)
public class CurrencyServiceBean implements CurrencyService {
    private static final Logger LOG = LoggerFactory.getLogger(CurrencyServiceBean.class);
    private static final String SERVICE_URL = "https://api.exchangeratesapi.io/latest?base={from}&symbols={to}";

    @PostConstruct
    public void init() {
        LOG.info("Currency service started");
    }

    @Override
    public Double  getCurrency(String from, String to) {
        RestTemplate template = new RestTemplate();

        Map<String, String> parameters = new HashMap<>();
        parameters.put("from", from);
        parameters.put("to", to);
        final Resp currency = template.getForObject(SERVICE_URL, Resp.class, parameters);
        if (currency == null) {
            throw new IllegalStateException();
        }
        final Map<String, Double> rates = currency.getRates();
        final Double currencyValue = rates.get(to);
        if (currencyValue == null) {
            throw new IllegalStateException();
        }
        return currencyValue;
    }
}