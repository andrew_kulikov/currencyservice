package com.company.currencyservice.web.screens;

import javax.inject.Inject;

import com.company.currencyservice.service.CurrencyService;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

@UiController("currencyservice_CurencyScreen")
@UiDescriptor("curency-screen.xml")
public class CurencyScreen extends Screen {
    @Inject
    private Label<Double> resultLabel;

    @Inject
    private CurrencyService currencyService;

    @Subscribe("show_currency")
    public void onShow_currencyClick(Button.ClickEvent event) {
        final Double eur = currencyService.getCurrency("USD", "RUB");
        resultLabel.setValue(eur);
    }

}